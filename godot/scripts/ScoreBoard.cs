using Godot;
using System;

public class ScoreBoard : RichTextLabel
{
    private int blue = 0;
    private int red = 0;

    public override void _Ready() {
        base._Ready();
        bakeText();
    }

    public void addBlue () {
        blue++;
        bakeText();
    }

    public void addRed () {
        red++;
        bakeText();
    }

    private void bakeText () {
        BbcodeText = "[center][color=blue]" + blue + "[/color]     [color=red]" + red + "[/color][/center]";
    }
}

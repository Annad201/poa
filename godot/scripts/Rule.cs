using System;
using System.Collections.Generic;

public class Rule {
    public struct Predicate {
        Func<Object, bool> f;
        Object arg;
        public Predicate(Func<Object, bool> function, Object argument) {
            f = function;
            arg = argument;
        }
        public bool eval() {
            return f(arg);
        } 
    }

    private List<Predicate> predicateList = new List<Predicate>();
    private Func<float, bool> action;
    
    public Rule (Func<float, bool> a) {
        action = a;
    }

    public Rule (Func<float, bool> a, Predicate p) : this(a) {
        addPredicate(p);
    }

    public Rule (Func<float, bool> a, List<Predicate> pl) : this(a) {
        addPredicate(pl);
    }

    public void addPredicate (Predicate p) {
        predicateList.Add(p);
    }

    public void addPredicate (List<Predicate> pl) {
        foreach (Predicate p in pl) {
            predicateList.Add(p);
        }
    }

    public Rule addPredicate (Func<Object, bool> function, Object argument) {
        predicateList.Add(new Predicate(function, argument));
        return this;
    }

    public Rule addPredicate (Func<Object, bool> function) {
        predicateList.Add(new Predicate(function, null));
        return this;
    }

    public Func<float, bool> eval() {
        foreach (Predicate p in predicateList) {
            if (!p.eval())
                return null;
        }
        return action;
    }
}
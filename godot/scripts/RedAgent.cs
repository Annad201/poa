using Godot;
using System;

public class RedAgent : Agent {

    public override void _Ready() {
        base._Ready();

        ChangeStateToDefense(0f);

        //Etats
        ruleList.Add(new Rule(ChangeStateToDefense)
        .addPredicate(isInState, "attack")
        .addPredicate(ballGoingTowardsRedGoal, null)
        );

        ruleList.Add(new Rule(ChangeStateToAttack)
        .addPredicate(isInState, "defense")
        .addPredicate(ballGoingTowardsBlueGoal)
        );

        //Aller en défense
        ruleList.Add(new Rule(turnLeft)
        .addPredicate(isInState, "defense")
        .addPredicate(eastLeft)
        );

        ruleList.Add(new Rule(turnRight)
        .addPredicate(isInState, "defense")
        .addPredicate(eastRight)
        );

        ruleList.Add(new Rule(moveLeft)
        .addPredicate(isInState, "defense")
        .addPredicate(ballLeft)
        );
        
        ruleList.Add(new Rule(moveRight)
        .addPredicate(isInState, "defense")
        .addPredicate(ballRight)
        );

        ruleList.Add(new Rule(moveBack)
        .addPredicate(isInState, "defense")
        .addPredicate(xPositionMoreThan, -3.4f)
        );

         //Attaquer
        ruleList.Add(new Rule(turnLeft)
        .addPredicate(isInState, "attack")
        .addPredicate(eastLeft)
        );

        ruleList.Add(new Rule(turnRight)
        .addPredicate(isInState, "attack")
        .addPredicate(eastRight)
        );

        ruleList.Add(new Rule(moveLeft)
        .addPredicate(isInState, "attack")
        .addPredicate(ballLeft)
        );
        
        ruleList.Add(new Rule(moveRight)
        .addPredicate(isInState, "attack")
        .addPredicate(ballRight)
        );

        ruleList.Add(new Rule(shoot)
        .addPredicate(isInState, "attack")
        .addPredicate(onEast)
        .addPredicate(canShoot)
        );

        ruleList.Add(new Rule(moveForward)
        .addPredicate(isInState, "attack")
        );
    }
}
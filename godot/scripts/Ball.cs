using Godot;
using System;

public class Ball : RigidBody
{

    BlueAgent blueAgent;
    RedAgent redAgent;
   
    public override void _Ready()
    {
        base._Ready();

        blueAgent = (BlueAgent)GetNode("../Blue");
        redAgent = (RedAgent)GetNode("../Red");
    }

    public override void _Process(float delta)
    {
        base._Process(delta);
        if (isInBlueGoal()) {
            goalRed();
        } else if (isInRedGoal()) {
            goalBlue();
        } else if (outOfBounds()) {
            resetBall();
        }
    }


    public bool outOfBounds ()
    {
        return Math.Abs(GlobalTransform.origin.x) > (AgentTickController.maxLenght + 0.2f )|| Math.Abs(GlobalTransform.origin.z) > (AgentTickController.maxWidth + 0.2f);
    }

    public bool isInRedGoal () {
        return GlobalTransform.origin.x < -3.5;
    }

    public bool isInBlueGoal () {
        return GlobalTransform.origin.x > 3.5;
    }

    public bool resetBall()
    {
        Random rngx = new Random();
        double rndx = rngx.NextDouble() - 0.5;

        Random rngz = new Random();
        double rndz = rngz.NextDouble() - 0.5;

        Transform t = GlobalTransform;
        t.origin = new Vector3((float) rndx, 0.1f, (float) rndz);
        GlobalTransform = t;

        LinearVelocity = Vector3.Zero;

        return true;
    }

    public bool goalBlue() {
        resetBall();
        blueAgent.reset(0);
        redAgent.reset(0);
        (GetNode("../ScoreBoard") as ScoreBoard).addBlue();
        return true;
    }

    public bool goalRed() {
        resetBall();
        blueAgent.reset(0);
        redAgent.reset(0);
        (GetNode("../ScoreBoard") as ScoreBoard).addRed();
        return true;
    }

    public bool shoot (Agent agent) {
        Vector3 direction = GlobalTransform.origin - agent.GlobalTransform.origin;
        direction = direction.Normalized();
        direction.y += 0.1f;

        ApplyImpulse(Vector3.Zero, direction * 5);

        System.Console.WriteLine("shoot");

        return true;
    }
}

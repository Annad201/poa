using System;
using System.Collections.Generic;
using Godot;

public class AgentTickController : Node {
    public static List<Agent> agentList = new List<Agent>();
    [Export] float tickEvery = 0.5f;
    [Export] bool continuous = true;
    float timer;

    public static float maxLenght;
    public static float maxWidth;

    public override void _Ready() {
        base._Ready();
        timer = tickEvery;

        CollisionShape cs = (CollisionShape)GetTree().Root.GetNode("/root/AgentTickController/Terrain/Ground/CollisionShape");
        BoxShape cube = (BoxShape)cs.Shape;
        Vector3 taille = cube.Extents;

        maxLenght = taille.x;
        maxWidth = taille.z;
    }

    public override void _Process(float delta) {
        if (continuous) { //Continuous refresh rate
            foreach (Agent a in agentList) {
                a.Run(delta);
            }
        } else  { //Frame by frame refresh rate
            if (timer <= 0) {
                timer = tickEvery;

                foreach (Agent a in agentList) {
                    a.Run(1);
                }
            }
            timer -= delta;
        }
    }   
}
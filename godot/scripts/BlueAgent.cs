using Godot;
using System;

public class BlueAgent : Agent {
    Agent red;

    public override void _Ready() {
        base._Ready();

        red = (Agent)GetParent().GetNode("Red");

        //Tirer
        ruleList.Add(new Rule(shoot)
        .addPredicate(this.canShoot, null)
        .addPredicate(this.ballFront, null)
        .addPredicate(this.facingWest, null)
        .addPredicate(this.ballLessThan, 0.7f));

        //Suivre la balle
        ruleList.Add(new Rule(turnRight)
        .addPredicate(ballBack, null)
        );

        ruleList.Add(new Rule(turnLeft)
        .addPredicate(ballLeft, null)
        );

        ruleList.Add(new Rule(turnRight)
        .addPredicate(ballRight, null)
        );

        //Contourner gardien
        ruleList.Add(new Rule(moveLeft)
        .addPredicate(onNorth, null)
        .addPredicate(facingWest, null)
        .addPredicate(ballBehindAgent, red)
        );

        ruleList.Add(new Rule(moveRight)
        .addPredicate(facingWest, null)
        .addPredicate(ballBehindAgent, red)
        );


        //Avancer
        ruleList.Add(new Rule(moveForward)
        .addPredicate(facingWest, null)
        .addPredicate(onMiddleW, null)
        .addPredicate(ballFront, null)
        );
        
        //Contournement

        ruleList.Add(new Rule(moveRight)
        .addPredicate(ballLessThan, 0.6f)
        .addPredicate(facingWest, null)
        .addPredicate(onNorth, null)
        );

         ruleList.Add(new Rule(moveLeft)
        .addPredicate(ballLessThan, 0.6f)
        .addPredicate(facingWest, null)
        .addPredicate(onSouth, null)
        );

        ruleList.Add(new Rule(moveLeft)
        .addPredicate(this.ballLessThan, 0.6f)
        .addPredicate(facingSouth, null)
        );

        ruleList.Add(new Rule(moveLeft)
        .addPredicate(this.ballLessThan, 0.6f)
        .addPredicate(facingEast, null)
        );

        ruleList.Add(new Rule(moveRight)
        .addPredicate(this.ballLessThan, 0.6f)
        .addPredicate(facingNorth, null)
        );


        //Default Rule

        ruleList.Add(new Rule(moveForward)
        .addPredicate(this.ballFront, null)
        );
    }
}
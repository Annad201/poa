using Godot;
using System;
using System.Collections.Generic;

public class Agent : KinematicBody {
    Ball ball;
    float shootTimer = 0;
    [Export] float speed = 1f;
    [Export] float turnSpeed = 15f;
    String state = "";

    Transform defaultTransform;


    public List<Rule> ruleList = new List<Rule>();

    public override void _Ready()
    {
        base._Ready();

        defaultTransform = GlobalTransform;

        ball = (Ball)GetTree().Root.GetNode("/root/AgentTickController/Ball");
        AgentTickController.agentList.Add(this);
    }

    public void Run(float delta) {
        if (shootTimer > 0)
            shootTimer -= delta;
        
        foreach (Rule r in ruleList) {
            Func<float, bool> f = r.eval();
            if (f != null) {
                System.Console.WriteLine(f.Method.Name + " : " + Name);
                f(delta);
                return;
            }
        }
    }


    //Predicats

    public bool zPositionLessThan(System.Object pos) {
        float p = (float)pos;
        return GlobalTransform.origin.z < p;
    }

    public bool zPositionMoreThan(System.Object pos) {
        float p = (float)pos;
        return GlobalTransform.origin.z > p;
    }

    public bool xPositionLessThan(System.Object pos) {
        float p = (float)pos;
        return GlobalTransform.origin.x < p;
    }

    public bool xPositionMoreThan(System.Object pos) {
        float p = (float)pos;
        return GlobalTransform.origin.x > p;
    }

    public bool facingEast(System.Object o) {
        return Transform.basis.GetEuler().y < -0.785f && Transform.basis.GetEuler().y > -2.356f;
    }

    public bool facingWest(System.Object o) {
        return Transform.basis.GetEuler().y > 0.785f && Transform.basis.GetEuler().y < 2.356f;
    }

    public bool facingSouth(System.Object o) {
        return Transform.basis.GetEuler().y <= -2.356f || Transform.basis.GetEuler().y >= 2.356f;
    }

    public bool facingNorth(System.Object o) {
        return Transform.basis.GetEuler().y >= -0.785f && Transform.basis.GetEuler().y <= 0.785f;
    }

    public bool westRight(System.Object o) {
        return Transform.basis.GetEuler().y < 1.5f; 
    }

    public bool westLeft(System.Object o) {
        return Transform.basis.GetEuler().y > 1.7f; 
    }

    public bool eastRight(System.Object o) {
        return Transform.basis.GetEuler().y > -1.5f;
    }

    public bool eastLeft(System.Object o) {
        return Transform.basis.GetEuler().y < -1.7f;
    }

    public bool onNorth (System.Object o) {
        return zPositionLessThan(-1f);
    }

    public bool onMiddleW (System.Object o) {
        return !onSouth(null) &&  !onNorth(null);
    }

    public bool onSouth (System.Object o) {
        return zPositionMoreThan(1f);
    }


    public bool onEast (System.Object o) {
        return xPositionMoreThan(2f);
    }

    public bool onMiddleH (System.Object o) {
        return !onEast(null) && !onWest(null);
    }

    public bool onWest (System.Object o) {
        return xPositionLessThan(-2f);
    }

    public bool ballLeft (System.Object o) {
        Vector3 localPoint = ToLocal(ball.GlobalTransform.origin);
        return localPoint.x < -0.1f;
    }

    public bool ballRight (System.Object o) {
        Vector3 localPoint = ToLocal(ball.GlobalTransform.origin);
        return localPoint.x > 0.1f;
    }

    public bool ballFront (System.Object o) {
        Vector3 localPoint = ToLocal(ball.GlobalTransform.origin);
        return localPoint.z < -0.1f;
    }

    public bool ballBack (System.Object o) {
        Vector3 localPoint = ToLocal(ball.GlobalTransform.origin);
        return localPoint.z > 0.1f;
    }

    public bool ballBehindAgent (System.Object o) {
        Agent a = (Agent)o;

        Vector3 ballLocal = ToLocal(ball.GlobalTransform.origin);
        Vector3 agentLocal = ToLocal(a.GlobalTransform.origin);

        if (ballLocal.Length() > agentLocal.Length()) {
            float dotProduct = ballLocal.Normalized().Dot(agentLocal.Normalized()); //Dot product = 1 if 2 vectors are colinear, 0 if  perpendicular
            return dotProduct > 0.8;
        }

        return false;
    }

    public bool ballLessThan (System.Object o) {
        Vector3 ballPos = ball.GlobalTransform.origin;
        Vector3 myPos = GlobalTransform.origin;

        return (ballPos - myPos).Length() < (float)o;
    }

    public bool ballMoreThan (System.Object o) {
        Vector3 ballPos = ball.GlobalTransform.origin;
        Vector3 myPos = GlobalTransform.origin;

        return (ballPos - myPos).Length() >= (float)o;
    }

    public bool ballGoingTowardsBlueGoal (System.Object o) {
        return ball.LinearVelocity.x > 0.2f;
    }

    public bool ballGoingTowardsRedGoal (System.Object o) {
        return ball.LinearVelocity.x < -0.2f;
    }

    public bool canShoot (System.Object o) {
        return shootTimer <= 0;
    }

    public bool isInState (System.Object o) {
        return state.Equals((String)o);
    }


    //Actions

    public bool reset (float delta) {
        GlobalTransform = defaultTransform;
        return true;
    }

    public bool moveForward (float delta) {
        MoveAndSlide(-GlobalTransform.basis.z * speed);
        return true;
    }

    public bool moveBack (float delta) {
        MoveAndSlide(GlobalTransform.basis.z * speed);
        return true;
    }

    public bool moveLeft (float delta) {
        MoveAndSlide(-GlobalTransform.basis.x * speed);
        return true;
    }

    public bool moveRight (float delta) {
        MoveAndSlide(GlobalTransform.basis.x * speed);
        return true;
    }

    public bool turnRight (float delta) {
        Rotate(Vector3.Up, delta * -turnSpeed);
        return true;
    }

    public bool turnLeft (float delta) {
        Rotate(Vector3.Up, delta * turnSpeed);
        return true;
    }

    public bool shoot (float delta) {
        ball.shoot(this);
        shootTimer = 2;
        return true;
    }

    public bool ChangeStateToAttack(float delta) {
        state = "attack";
        return true;
    }

    public bool ChangeStateToDefense(float delta) {
        state = "defense";
        return true;
    }
    
}